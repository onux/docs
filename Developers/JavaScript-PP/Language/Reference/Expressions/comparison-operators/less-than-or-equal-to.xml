<keyword>
	<menu file="Developers" />
	<title>Less Than or Equal To (&lt;=) Comparison Operator</title>
	<summary>
	Returns true if the left operand is less than or equal to the right operand.
	</summary>
	<syntax>
expression1 &lt;= expression2
	</syntax>
	<parameters>
		<param name="expression1">
		Any legal expression.
		</param>
		<param name="expression2">
		Any legal expression.
		</param>
	</parameters>
	<description><![CDATA[
	<p>The "less than or equal to" operator compares the values of two expressions. If the expression on the left evaluates to a value that is less than or equal to the evaluated value of the expression on the right, the "less than or equal to" operator returns <code>true</code>; otherwise, the operator returns <code>false</code>.</p>

	<p>If either expression has a JavaScript type (such as symbols declared with <ref to="Developers/JavaScript-PP/Language/Reference/Statements/var">var</ref>, <ref to="Developers/JavaScript-PP/Language/Reference/Statements/function">function</ref>, or <ref to="Developers/JavaScript-PP/Language/Reference/Statements/external">external</ref>), the "less than or equal to" operator will additionally - at runtime - convert the values of the expressions to the same type before comparing the values of the results.</p>
]]>
	</description>
	<examples>
		<example name="Comparing Values">
<![CDATA[
import System;

int a = 1, b = 1, c = 2, d = 3;
Console.log(a &lt;= b); // true
Console.log(b &lt;= c); // true
Console.log(d &lt;= c); // false
]]>
		</example>
		<example name="Comparing Values in an 'if' statement">
<![CDATA[
import System;

int x = 1;

if (x &lt;= 2) {
	Console.log("x is less than or equal to 2");
}
else {
	Console.log("x is not less than or equal to 2");
}
]]>
		</example>
	</examples>
    <see>
        <ref to="Developers/JavaScript-PP/Language/Reference/Expressions/true">true</ref>
        <ref to="Developers/JavaScript-PP/Language/Reference/Expressions/false">false</ref>
        <ref to="Developers/JavaScript-PP/Language/Reference/Expressions/comparison-operators/less-than">Less Than Comparison Operator</ref>
        <ref to="Developers/JavaScript-PP/Language/Reference/Expressions/comparison-operators/greater-than">Greater Than Comparison Operator</ref>
        <ref to="Developers/JavaScript-PP/Language/Reference/Expressions/comparison-operators/greater-than-or-equal-to">Greater Than or Equal To Comparison Operator</ref>
    </see>
</keyword>