<keyword>
	<menu file="Developers" />
	<title>Equality (==) Comparison Operator</title>
	<summary>
	Compares two expressions for value equality.
	</summary>
	<syntax>
expression1 == expression2
	</syntax>
	<parameters>
		<param name="expression1">
		Any legal expression.
		</param>
		<param name="expression2">
		Any legal expression.
		</param>
	</parameters>
	<description><![CDATA[
	<p>The equality operator compares the values of two expressions. The equality operator returns <code>true</code> if the resulting values of the expressions are equal and <code>false</code> otherwise.</p>

	<p>If your code uses JavaScript code (such as symbols declared with <ref to="Developers/JavaScript-PP/Language/Reference/Statements/var">var</ref>, <ref to="Developers/JavaScript-PP/Language/Reference/Statements/function">function</ref>, or <ref to="Developers/JavaScript-PP/Language/Reference/Statements/external">external</ref>), the equality operator will additionally - at runtime - convert the values of the expressions to the same type before comparing the values of the results.</p>
]]>
	</description>
	<examples>
		<example name="Comparing Values">
<![CDATA[
import System;

int x = 1, y = 1, z = 2;
Console.log(x == y); // true
Console.log(y == z); // false
]]>
		</example>
		<example name="Comparing Values in an 'if' statement">
<![CDATA[
import System;

int x = 1;

if (x == 1) {
	Console.log("x is 1");
}
else {
	Console.log("x is not 1");
}
]]>
		</example>
	</examples>
    <see>
        <ref to="Developers/JavaScript-PP/Language/Reference/Expressions/true">true</ref>
        <ref to="Developers/JavaScript-PP/Language/Reference/Expressions/false">false</ref>
        <ref to="Developers/JavaScript-PP/Language/Reference/Expressions/comparison-operators/strict-equality">Strict Equality Comparison Operator</ref>
        <ref to="Developers/JavaScript-PP/Language/Reference/Expressions/comparison-operators/inequality">Inequality Comparison Operator</ref>
        <ref to="Developers/JavaScript-PP/Language/Reference/Expressions/comparison-operators/strict-inequality">Strict Inequality Comparison Operator</ref>
    </see>
</keyword>