<keyword>
	<menu file="Developers" />
	<title>Concatenation Assignment (+=) Expression</title>
	<summary>
	Appends a string value to an existing variable, property, or array element.
	</summary>
	<syntax>
variable += expression
	</syntax>
	<parameters>
		<param name="variable">
		The variable, property, or array element to be given a value.
		</param>
		<param name="expression">
		Any legal expression.
		</param>
	</parameters>
	<description><![CDATA[
	<p>For string variables, the <code>+=</code> operator computes the string value of the expression on the right, and then appends it to the variable, property, or array element on the left. The expression on the right is evaluated prior to assignment.</p>

	<p>The <code>+=</code> operator is equivalent to the <code>+</code> operator with the left-hand side being the variable, property, or array element to modify and the right-hand side being the expression to concatenate. Therefore, the following expressions are equivalent:</p>

```jspp
x += "foo";    // Equivalent to x = x + "foo"
x = x + "foo"; // Equivalent to x += "foo"
```
]]>
	</description>
	<examples>
		<example name="Basic Usage">
<![CDATA[
import System;

string foo = "foo";
Console.log(foo); // "foo"
foo += "bar";
Console.log(foo); // "foobar"
]]>
		</example>
	</examples>
	<see>
        <ref to="Developers/JavaScript-PP/Language/Reference/Types/Primitive-Types/string"><code>string</code> Type</ref>
		<ref to="Developers/JavaScript-PP/Language/Reference/Expressions/string-concatenation-operator">String Concatenation Operator</ref>
		<ref to="Developers/JavaScript-PP/Language/Reference/Expressions/arithmetic-operators/addition">Addition Operator</ref>
	</see>
</keyword>