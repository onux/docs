<keyword>
	<menu file="Developers" />
	<title>Bitwise OR (|) Expression</title>
	<summary>
	Evaluate with a logical inclusive OR operation.
	</summary>
	<syntax>
expression1 | expression2
	</syntax>
	<parameters>
		<param name="expression1">
		Any legal Boolean or numeric expression.
		</param>
		<param name="expression2">
		Any legal Boolean or numeric expression.
		</param>
	</parameters>
	<description><![CDATA[
	<p>For numeric and Boolean expressions, the <code>|</code> operator computes the values of the operands, converts them to equal-length binary representations, performs a logical inclusive OR operation on them, and returns the evaluated result. For example, the decimal (base 10) number 0 (zero) may be converted to the binary form 0000 and the number 1 (one) may be converted to 0001. The bitwise OR operation will compare each bit. If both bits are 0 (zero), the resulting bit is 0 (zero); otherwise, the resulting bit is 1 (one).</p>

	<p>For example, here is an illustration of the evaluation of the expression <code>0 | 1</code>:</p>

```jspp
  0000 (0)
  0001 (1)
  ----
= 0001 (1)
```

	<p>Here is an illustration for the evaluation of the expression <code>3 | 5</code>:</p>

```jspp
  0011 (3)
  0101 (5)
  ----
= 0111 (7)
```

	<h3>Differences from JavaScript</h3>

	<p>Logical OR operations are not limited to 32-bit integers in JS++. <sup class="unimplemented">Unimplemented</sup> In JavaScript, the logical OR operation is limited to signed 32-bit integers.</p>
]]>
	</description>
	<examples>
		<example name="Basic Usage">
<![CDATA[
import System;

Console.log(0 | 0); // 0
Console.log(0 | 1); // 1
Console.log(1 | 5); // 5
]]>
		</example>
	</examples>
	<see>
		<ref to="Developers/JavaScript-PP/Language/Reference/Expressions/assignment-operators/bitwise-or">Bitwise OR Assignment Operator</ref>
		<ref to="Developers/JavaScript-PP/Language/Reference/Expressions/bitwise-operators/and">Bitwise AND Operator</ref>
		<ref to="Developers/JavaScript-PP/Language/Reference/Expressions/bitwise-operators/xor">Bitwise XOR Operator</ref>
	</see>
</keyword>