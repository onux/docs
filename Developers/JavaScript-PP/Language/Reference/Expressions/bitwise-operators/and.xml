<keyword>
	<menu file="Developers" />
	<title>Bitwise AND (&amp;) Expression</title>
	<summary>
	Evaluate with a logical AND operation.
	</summary>
	<syntax>
expression1 &amp; expression2
	</syntax>
	<parameters>
		<param name="expression1">
		Any legal Boolean or numeric expression.
		</param>
		<param name="expression2">
		Any legal Boolean or numeric expression.
		</param>
	</parameters>
	<description><![CDATA[
	<p>For numeric and Boolean expressions, the <code>&amp;</code> operator computes the values of the operands, converts them to equal-length binary representations, performs a logical AND operation on them, and returns the evaluated result. For example, the decimal (base 10) number 0 (zero) may be converted to the binary form 0000 and the number 1 (one) may be converted to 0001. The bitwise AND operation will compare each bit. If both bits are 1 (one), the resulting bit is 1 (one); otherwise, the resulting bit is 0 (zero).</p>

	<p>For example, here is an illustration of the evaluation of the expression <code>0 &amp; 1</code>:</p>

```jspp
  0000 (0)
  0001 (1)
  ----
= 0000 (0)
```

	<p>Here is an illustration for the evaluation of the expression <code>0 &amp; 5</code>:</p>

```jspp
  0001 (1)
  0101 (5)
  ----
= 0001 (1)
```

	<h3>Differences from JavaScript</h3>

	<p>Logical AND operations are not limited to 32-bit integers in JS++. <sup class="unimplemented">Unimplemented</sup> In JavaScript, the logical AND operation is limited to signed 32-bit integers.</p>
]]>
	</description>
	<examples>
		<example name="Basic Usage">
<![CDATA[
import System;

Console.log(0 &amp; 1); // 0
Console.log(1 &amp; 5); // 1
]]>
		</example>
	</examples>
	<see>
		<ref to="Developers/JavaScript-PP/Language/Reference/Expressions/assignment-operators/bitwise-and">Bitwise AND Assignment Operator</ref>
		<ref to="Developers/JavaScript-PP/Language/Reference/Expressions/bitwise-operators/or">Bitwise OR Operator</ref>
		<ref to="Developers/JavaScript-PP/Language/Reference/Expressions/bitwise-operators/xor">Bitwise XOR Operator</ref>
	</see>
</keyword>