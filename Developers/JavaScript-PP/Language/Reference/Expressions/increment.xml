<keyword>
    <menu file="Developers" />
	<title>Increment (++)</title>
	<summary>
	Increases the value of a variable by one (1).
	</summary>
	<syntax>
variable++
++variable
	</syntax>
	<parameters>
        <param name="variable">
        <![CDATA[Any numeric variable.]]>
        </param>
	</parameters>
	<description><![CDATA[
        <p><code>++</code> increments the specified variable by one (1). While the operator can be used in a standalone statement, it can also be included anywhere the variable is used, such as function calls or assignment statements. In those cases, if the <code>++</code>precedes the variable, it is known as the prefix increment operator and the variable is incremented before the statement is executed. If the <code>++</code> follows the variable, it is known as the postfix increment operator and the variable is incremented after the statement is executed.</p>
        
        <p>Note that in a <code>for</code> statement, the update component is always executed after the body of the loop. Thus, <code>++i</code> and <code>i++</code> have the same effect in this case.</p>
     ]]>
    </description>
    <examples>
        <example name="Standalone Use">
<![CDATA[
import System;

int x = 2;
++x;
Console.log(x); // 3
]]>
        </example>
        <example name="Increment Before">
<![CDATA[
import System;

int x = 2;
y = ++x;

Console.log(x); // 3
Console.log(y); // 3
]]>
        </example>
        <example name="Increment After">
<![CDATA[
import System;

int x = 2;
y = x++;

Console.log(x); // 3
Console.log(y); // 2
]]>
        </example>
        <example name="for Statement">
<![CDATA[
import System;

for (int i = 0; i < 5; ++i) {
    Console.log(i); // First iteration will be 0, last will be 4
}
]]>
        </example>
    </examples>
    <see>
        <ref to="Developers/JavaScript-PP/Language/Reference/Statements/for"><code>for</code> loop</ref>
        <ref to="Developers/JavaScript-PP/Language/Reference/Expressions/decrement">Decrement (--)</ref>
        <ref to="Developers/JavaScript-PP/Language/Reference/Expressions/assignment-operators/incremental">Incremental Assignment (+=) Operator</ref>
    </see>
</keyword>