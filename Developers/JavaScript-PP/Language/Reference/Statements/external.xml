<keyword>
    <menu file="Developers" />
	<title>external</title>
	<summary>
	Imports symbols from JavaScript. If declared with an initializer, the <code>external</code> keyword declares a variable with type <code>external</code> and initialized to an arbitrary value.
	</summary>
	<syntax>
external name1[= value1 [, name2 [, name3 ... [, nameN]]]];
	</syntax>
	<parameters>
		<param name="nameN">
		The name of the symbol to import from JavaScript. This can be any valid JavaScript identifier. If an initializer is present, no importing will happen and this will simply act as the name to assign to the initialized value.
		</param>
        <param name="valueN">
        Optional. Initializes the name to a value.
        </param>
	</parameters>
	<description><![CDATA[
        <p>The <code>external</code> keyword will import symbols from JavaScript. However, if the <code>external</code> keyword is used with an initializer, it will declare a variable of type <code>external</code> instead.</p>
        
        <p>The <code>external</code> keyword can only be used at the top-level scope of a file or inside a <ref to="Developers/JavaScript-PP/Language/Reference/Statements/module">module</ref>. Note that there is a difference between externals declared at the program level (top-level scope) and externals declared at the module level. Externals declared at the program level are scoped to the file, while externals declared inside a module will be exported with the module. Thus, modules that wish to utilize externals without exporting them should have the externals declared at the program level scope of the module file.</p>
<!--     
<pre class="brush:jspp">
external qux; // This will NOT be exported with the module

module Foo
{
    external bar; // This will be exported with the module
}
</pre>
-->

        <p>In addition, the <code>external</code> keyword cannot be modified after its declaration. In order to declare a variable with the external type but which can later be modified, use a non-<code>final</code> <ref to="Developers/JavaScript-PP/Language/Reference/Statements/var">var</ref>.</p>
    ]]>
    </description>
    <examples>
        <example name="Importing jQuery">
<![CDATA[
// jQuery declares two symbols: 'jQuery' and '$' so we import both
external jQuery, $;

$("#myelement").hide();
]]>
        </example>
        <example name="Importing Node.js Modules">
<![CDATA[
// Import 'require' first
external require;
// After 'require' has been imported, we can use it to import the 'fs' module
external fs = require("fs");

fs.writeFile("message.txt", "Hello World", void(err) {
    if (err) {
        throw err;
    }
});
]]>
        </example>
        <example name="jQuery AJAX Example">
<![CDATA[
// jQuery declares two symbols: 'jQuery' and '$' so we import both by convention
external jQuery, $;

$.ajax({
    url: "myfile.txt",
    success: void(string result) {
        result = "#BEGIN#" + result + "#END#";
        
        $("#myelement").text(result);
    }
});
]]>
        </example>
    </examples>
    <see>
        <ref to="Developers/JavaScript-PP/Language/Reference/Statements/var">var</ref>
        <ref to="Developers/JavaScript-PP/Language/Reference/Statements/function">function</ref>
        <ref to="Developers/JavaScript-PP/Language/Reference/Types/Primitive-Types/external">external type</ref>
    </see>
</keyword>