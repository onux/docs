<document>
    <title>Nullable Types</title>
    <menu file="Developers" />
    <summary>Enables internal types to take null values.</summary>
    <content>
        <![CDATA[
        <p>Nullable types enable internal JS++ types to accept null values.</p>

```jspp
int x = 1;
// x = null; // ERROR
int? y = 1;
y = null;    // OK
```

        <h3>Operators</h3>

        <p>JS++ provides three operators for working with nullable types: <code>??</code> (safe default operator), <code>?.</code> (safe navigation operator), and <code>?=</code> (safe assignment operator).</p>

        <strong>Safe Default Operator</strong>

        <p>The <ref to="Developers/JavaScript-PP/Language/Reference/Expressions/safe-default-operator">safe default operator</ref> can be used for providing an alternative value if a null value is encountered:</p>

```jspp
int? x = null;
int? y = x ?? 5; // y == 5
```

        <strong>Safe Navigation Operator</strong>

        <p>The <ref to="Developers/JavaScript-PP/Language/Reference/Expressions/safe-navigation-operator">safe navigation operator</ref> is similar to the <code>.</code> operator except it only looks up class members if the object is not null.</p>

```jspp
import System;

class Foo
{
    Foo? maybe() {
        if (Math.random() > 0.5) {
            return this;
        }
        else {
            return null;
        }
    }

    void log() {
        Console.log("Something happened.");
    }
}

auto foo = new Foo();
foo.maybe()?.log();
```

        <p>In the above code, <code>maybe</code> can return either an instance of the class or <code>null</code>. By using the safe navigation operator (<code>?.</code>), the <code>log</code> method will only be called if the class instance was returned by <code>maybe</code>.</p>

        <strong>Safe Assignment Operator</strong>

        <p>The <ref to="Developers/JavaScript-PP/Language/Reference/Expressions/assignment-operators/safe">safe assignment operator</ref> can be used to assign a different value if a <code>null</code> value is present:</p>

```jspp
import System;

int? x = null;
Console.log(x); // null
x ?= 5;
Console.log(x); // 5
```

        <h3>Casting</h3>

        <p>Nullable types can be stripped away via casting:</p>

```jspp
class Foo {}

Foo? foo = new Foo();
Foo bar = (Foo) foo;
```

        <p>Casting can throw a <ref to="Developers/JavaScript-PP/Standard-Library/System/Exceptions/CastException">CastException</ref>. Therefore, it is preferred to use the nullable operators instead. If casting is absolutely necessary, it is preferred to perform a check first:</p>

```jspp
import System;

class Foo {}

Foo? foo;
if (Math.random() > 0.5) {
    foo = new Foo();
}
else {
    foo = null;
}

if (foo != null) { // this check makes sure the cast is safe
    Foo bar = (Foo) foo;
}
```

        <p>Unlike casting from other types, casting from nullable or existent types will only strip the nullable or existent type. It will not simultaneously allow downcasting. If you desire to downcast, you need to downcast separately.</p>
        ]]>
    </content>
    <see>
        <ref to="Developers/JavaScript-PP/Language/Reference/Expressions/null">null</ref>
    </see>
</document>
