<class><title><![CDATA[Controller]]></title><menu file="Developers" /><modifiers><modifier name="abstract" /></modifiers><summary><![CDATA[<p>The abstract base class for controllers.</p>]]></summary><description><![CDATA[<p>The <code>Controller</code> abstract class is the abstract base class for
controllers in Altitude MVC/MVP applications.</p>

<p>Attaching views to models (via the observer pattern) and listening for
user input events are done in individual methods to enable easy testing.
In your application code, attach views and listen for events; in your
test code, do not attach the views and do not listen for events.</p>]]></description><examples><example name="Basic Usage"><![CDATA[import Altitude.Frontend;

external $;

class CustomersController : Controller
{
    Views.Customers view = new Views.Customers();
    Models.Customers model = new Models.Customers();

    public override void attachViews() {
        this.model.attach(this.view);
    }

    public override void listenEvents() {
        $("#customers").click(function() {
            // ...
        });
    }
}

CustomersController customersController = new CustomersController();
customersController.attachViews();  // Do not do this in your tests
customersController.listenEvents(); // Do not do this in your tests]]></example><example name="Decoupling Controller from the Web Browser / DOM Input Events"><![CDATA[import Altitude.Frontend;
import Altitude.Frontend.ViewEngines;

class CustomersController : Controller
{
    IViewEngine viewEngine;
    Views.Customers view = new Views.Customers();
    Models.Customers model = new Models.Customers();

    // Use dependency injection to allow applications to specify the
    // concrete implementation of `IViewEngine` to use (such as
    // `BrowserViewEngine` for web browsers).
    public CustomersController(IViewEngine viewEngine) {
        this.viewEngine = viewEngine;
    }

    public override void attachViews() {
        this.model.attach(this.view);
    }

    public override void listenEvents() {
        viewEngine.get("customers").on("click", function() {
            // ...
        });
    }
}]]></example></examples><see></see></class>