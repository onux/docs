<method><title><![CDATA[fromStringOrThrow]]></title><menu file="Developers" /><summary><![CDATA[<p>Converts a <code>string</code> to a valid <code>bool</code> value or throws an exception if the value cannot be converted.</p>]]></summary><overload><description><![CDATA[<p>Converts a <code>string</code> to a valid <code>bool</code> value or throws an exception
if the value cannot be converted. If the <code>string</code> value is
not a Boolean <code>true</code> or <code>false</code> value,
<code>System.Exceptions.InvalidBooleanException</code> will be thrown.</p>

<p>For <code>string</code> values that are not Boolean (<code>true</code> or <code>false</code>),
<code>System.Exceptions.InvalidBooleanException</code> is thrown:</p>

<pre><code class="jspp">import System;
import System.Exceptions;

try {
    bool x = Boolean.fromStringOrThrow(&quot;true&quot;);
}
catch(InvalidBooleanException e) {
    // Will not execute because the string value is a valid Boolean
    Console.error(e);
}

try {
    bool y = Boolean.fromStringOrThrow(&quot;abc&quot;);
}
catch(InvalidBooleanException e) {
    Console.error(e);
}
</code></pre>]]></description><modifiers><modifier name="public" /><modifier name="static" /></modifiers><return type="bool"><![CDATA[<p>The <code>bool</code> equivalent of the string value if the
         string is a valid <code>bool</code> value.</p>]]></return><params><param name="boolAsStr" type="string"><![CDATA[<p>The string value to convert.</p>]]></param></params><examples><example name="Valid Conversion (No exception)"><![CDATA[import System;
import System.Exceptions;

bool valid1 = Boolean.fromStringOrThrow("true");
Console.log(valid1); // true
bool valid2 = Boolean.fromStringOrThrow("false");
Console.log(valid2); // false]]></example><example name="Invalid Conversion (Throws exceptions)"><![CDATA[import System;

try {
    bool invalid = Boolean.fromStringOrThrow("abc");
}
catch(InvalidBooleanException e) {
    Console.error(e);
}]]></example></examples><see></see></overload></method>