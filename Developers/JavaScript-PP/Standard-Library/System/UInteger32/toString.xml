<method><title><![CDATA[toString]]></title><menu file="Developers" /><summary><![CDATA[<p>Converts the <code>unsigned int</code> value to an equivalent <code>string</code>
          value.</p>]]></summary><overload><description><![CDATA[<p>Converts the <code>unsigned int</code> value to an equivalent <code>string</code> value.</p>

<p>For instance, <code>123</code> will become <code>&quot;123&quot;</code>.</p>]]></description><modifiers><modifier name="public" /><modifier name="override" /></modifiers><return type="string"><![CDATA[<p>The equivalent <code>string</code> value for the <code>unsigned int</code> value.</p>]]></return><params></params><examples><example name="Basic Usage"><![CDATA[import System;

unsigned int a = 1;
unsigned int b = 2;
Console.log(a.toString()); // "1"
Console.log(b.toString()); // "2"]]></example><example name="Concatentation of Stringified Values"><![CDATA[import System;

unsigned int a = 1;
unsigned int b = 2;
Console.log(a.toString() + b.toString());       // "12"
Console.log(a.toString() + ", " + b.toString()); // "1, 2"]]></example></examples><see></see></overload><overload><description><![CDATA[<p>Returns a string representation of the integer by converting the
integer value from base 10 to the specified base.</p>

<p><code>System.Exceptions.OutOfRangeException</code> will be thrown if the
supplied argument for the base is not an integer between 2 and 36
(inclusive).</p>]]></description><deprecated><![CDATA[<p>Use <code>System.UInteger32.toBase</code> instead.</p>]]></deprecated><modifiers><modifier name="public" /></modifiers><return type="string"><![CDATA[<p>A string representation of the integer value.</p>]]></return><params><param name="base" type="int"><![CDATA[<p>The base to convert the integer value to. Must be
             between 2-36.</p>]]></param></params><examples><example name="Basic Usage"><![CDATA[unsigned int a = 97;
string base8  = a.toString(8);  // "141"
string base16 = a.toString(16); // "61"]]></example><example name="Converting integer to string"><![CDATA[unsigned int a = 97;
string s = a.toString(10); // "97"]]></example></examples><see></see></overload></method>