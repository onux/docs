<method><title><![CDATA[fromString]]></title><menu file="Developers" /><summary><![CDATA[<p>Converts a <code>string</code> to an <code>unsigned short</code> value.</p>]]></summary><overload><description><![CDATA[<p>Converts a <code>string</code> to an <code>unsigned short</code> value.</p>

<pre><code class="jspp">import System;

unsigned short x = UInteger16.fromString(&quot;100&quot;);
Console.log(x); // 100
</code></pre>

<p>If the string being converted is not a valid integer value, zero
(<code>0</code>) is returned.</p>

<pre><code class="jspp">import System;

unsigned short x = UInteger16.fromString(&quot;abc&quot;);
Console.log(x); // 0
</code></pre>

<p>If the string being converted is numeric but does not fit into the
<code>unsigned short</code> data range <code>[ 0, 65535 ]</code> (inclusive), the value
will be wrapped: if the value exceeds the <code>unsigned short</code> maximum
(<code>65,535</code>) by one, it will wrap around to the <code>unsigned short</code>
minimum (<code>0</code>); if the value exceeds the <code>unsigned short</code> maximum by
two, it will wrap around to the <code>unsigned short</code> minimum plus one
(<code>1</code>); and so on.</p>

<pre><code class="jspp">import System;

unsigned short exceedByOne = UInteger16.fromString(&quot;65536&quot;);
Console.log(exceedByOne); // 0
unsigned short exceedByTwo = UInteger16.fromString(&quot;65537&quot;);
Console.log(exceedByTwo); // 1
unsigned short exceedByThree = UInteger16.fromString(&quot;65538&quot;);
Console.log(exceedByThree); // 2
</code></pre>]]></description><modifiers><modifier name="public" /><modifier name="static" /></modifiers><return type="unsigned short"><![CDATA[<p>The <code>unsigned short</code> equivalent of the string value or zero
         (<code>0</code>) if the string is not a numeric value.</p>]]></return><params><param name="numberAsStr" type="string"><![CDATA[<p>The string value to convert.</p>]]></param></params><examples><example name="Basic Usage"><![CDATA[import System;

unsigned short x = UInteger16.fromString("100");
Console.log(x); // 100]]></example></examples><see></see></overload></method>