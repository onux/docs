<method><title><![CDATA[random]]></title><menu file="Developers" /><summary><![CDATA[<p>Generates a random number.</p>]]></summary><overload><description><![CDATA[<p>Generates a random number greater than or equal to zero (0) and less
than one (1). [0...1)</p>]]></description><modifiers><modifier name="public" /><modifier name="static" /></modifiers><return type="double"><![CDATA[<p>A random number greater than or equal to zero (0) and less
         than one (1). [0...1)</p>]]></return><params></params><examples><example name="Basic Usage"><![CDATA[import System;

Console.log(Math.random());]]></example></examples><see></see></overload><overload><description><![CDATA[<p>Generates a random, whole 32-bit integer number greater than or equal
to zero (0) and less than n. [0...n)</p>]]></description><modifiers><modifier name="public" /><modifier name="static" /></modifiers><return type="int"><![CDATA[<p>A whole number greater than or equal to zero (0) and less
         than n. [0...n)</p>]]></return><params><param name="n" type="int"><![CDATA[<p>The upper limit for the random number generation
          (non-inclusive).</p>]]></param></params><examples><example name="Basic Usage"><![CDATA[import System;

// Generate a random number between 0 and 99 (inclusive)
Console.log(Math.random(100));]]></example></examples><see></see></overload><overload><description><![CDATA[<p>Generates a random, whole 32-bit integer number greater than or equal
to start and less than stop. [start...stop)</p>]]></description><modifiers><modifier name="public" /><modifier name="static" /></modifiers><return type="int"><![CDATA[<p>A whole number greater than or equal to <code>start</code> and less than
         <code>stop</code>. [<code>start</code>...<code>stop</code>)</p>]]></return><params><param name="start" type="int"><![CDATA[<p>The lower limit for the random number generation
              (inclusive).</p>]]></param><param name="stop" type="int"><![CDATA[<p>The upper limit for the random number generation
             (non-inclusive).</p>]]></param></params><examples><example name="Basic Usage"><![CDATA[import System;

// Generate a random number between 1 and 100 (inclusive)
Console.log(Math.random(1, 101));]]></example></examples><see></see></overload><overload><description><![CDATA[<p>Generates a random number greater than or equal to zero (0) and less
than n. [0...n)</p>]]></description><modifiers><modifier name="public" /><modifier name="static" /></modifiers><return type="double"><![CDATA[<p>A random value greater than or equal to zero (0) and less
         than n. [0...n)</p>]]></return><params><param name="n" type="double"><![CDATA[<p>The upper limit for the random number generation
          (non-inclusive).</p>]]></param></params><examples><example name="Basic Usage"><![CDATA[import System;

Console.log(Math.random(2.2d));]]></example></examples><see></see></overload><overload><description><![CDATA[<p>Generates a random number greater than or equal to start and less
than to stop. [start...stop)</p>]]></description><modifiers><modifier name="public" /><modifier name="static" /></modifiers><return type="double"><![CDATA[<p>A random number greater than or equal to <code>start</code> and less
         than <code>stop</code>. [<code>start</code>...<code>stop</code>)</p>]]></return><params><param name="start" type="double"><![CDATA[<p>The lower limit for the random number generation
              (inclusive).</p>]]></param><param name="stop" type="double"><![CDATA[<p>The upper limit for the random number generation
             (non-inclusive).</p>]]></param></params><examples><example name="Basic Usage"><![CDATA[import System;

Console.log(Math.random(1.1d, 2.2d));]]></example></examples><see></see></overload></method>