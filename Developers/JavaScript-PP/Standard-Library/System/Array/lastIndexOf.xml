<method><title><![CDATA[lastIndexOf]]></title><menu file="Developers" /><summary><![CDATA[<p>Returns the last index of the search element in the array.</p>]]></summary><overload><description><![CDATA[<p>Returns the last index of the search element in the array. By
default, elements are searched by value for primitive data types and
by reference otherwise. This behavior can be overridden by
implementing the <code>System.ILike&lt;T&gt;</code> interface.</p>

<p>This method does not mutate the array.</p>

<p>This method was standardized in ECMAScript 5 for JavaScript. For web
browsers that do not support ECMAScript 5, JS++ will provide a
polyfill for this method only if it is used.</p>]]></description><modifiers><modifier name="public" /></modifiers><return type="int"><![CDATA[<p>The last index of the search element in the array or <code>-1</code> if
         the element could not be found in the array.</p>]]></return><params><param name="searchElement" type="T"><![CDATA[<p>The element to search for.</p>]]></param></params><examples><example name="Basic Usage"><![CDATA[import System;

int[] arr = [ 1, 2, 3, 1 ];
Console.log(arr.indexOf(1));     // 0
Console.log(arr.lastIndexOf(1)); // 3
Console.log(arr.lastIndexOf(2)); // 1
Console.log(arr.lastIndexOf(3)); // 2
Console.log(arr.lastIndexOf(4)); // -1]]></example></examples><see></see></overload><overload><description><![CDATA[<p>Returns the last index of the search element in the array until the
specified ending index. By default, elements are searched by value
for primitive data types and by reference otherwise. This behavior
can be overridden by implementing the <code>System.ILike&lt;T&gt;</code>
interface.</p>

<p>This method does not mutate the array.</p>

<p>This method was standardized in ECMAScript 5 for JavaScript. For web
browsers that do not support ECMAScript 5, JS++ will provide a
polyfill for this method only if it is used.</p>]]></description><modifiers><modifier name="public" /></modifiers><return type="int"><![CDATA[<p>The last index of the search element in the array or <code>-1</code> if
         the element could not be found in the array.</p>]]></return><params><param name="searchElement" type="T"><![CDATA[<p>The element to search for.</p>]]></param><param name="endingIndex" type="int"><![CDATA[<p>The zero-based index to limit and end the search
                    at. This argument can be negative to specify an
                    index relative to the end of the array.</p>]]></param></params><examples><example name="Basic Usage"><![CDATA[import System;

int[] arr = [ 1, 2, 3, 1 ];
Console.log(arr.indexOf(1));        // 0
Console.log(arr.lastIndexOf(1));    // 3
Console.log(arr.lastIndexOf(1, 1)); // 0
Console.log(arr.lastIndexOf(1, 3)); // 3]]></example><example name="Search until the penultimate element"><![CDATA[import System;

int[] arr = [ 1, 2, 3, 1 ];
Console.log(arr.lastIndexOf(1, -2)); // 0]]></example></examples><see></see></overload></method>