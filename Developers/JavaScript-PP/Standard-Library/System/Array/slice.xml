<method><title><![CDATA[slice]]></title><menu file="Developers" /><summary><![CDATA[<p>Returns a subset of the array beginning at the specified index until the end of the array.</p>]]></summary><overload><description><![CDATA[<p>Returns a shallow copy representing the subset of the array beginning
at the specified index until the end of the array.</p>

<p>This method does not mutate the array.</p>]]></description><modifiers><modifier name="public" /></modifiers><return type="T[]"><![CDATA[<p>A shallow copy representing the subset of the array beginning
         at the specified index until the end of the array.</p>]]></return><params><param name="begin" type="int"><![CDATA[<p>Zero-based index to begin extraction from. This argument
              can be negative to begin the <code>slice</code> from the end of the
              array.</p>]]></param></params><examples><example name="Slicing from the beginning of the array"><![CDATA[import System;

[ 1, 2, 3, 4, 5 ].slice(1); // [ 2, 3, 4, 5 ]]]></example><example name="Slicing from the end of the array"><![CDATA[import System;

[ 1, 2, 3, 4, 5 ].slice(-2); // [ 4, 5 ]]]></example></examples><see></see></overload><overload><description><![CDATA[<p>Returns a shallow copy representing the subset of the array beginning
at the specified starting index (zero-based) to the specified ending
index (one-based).</p>

<p>This method does not mutate the array.</p>]]></description><modifiers><modifier name="public" /></modifiers><return type="T[]"><![CDATA[<p>A shallow copy representing the subset of the array beginning
         at the specified starting index until the specified ending
         index.</p>]]></return><params><param name="begin" type="int"><![CDATA[<p>Zero-based index to begin extraction from. This argument
              can be negative to begin the <code>slice</code> from the end of the
              array.</p>]]></param><param name="end" type="int"><![CDATA[<p>One-based index to end extraction at. This argument can be
            negative to end the <code>slice</code> at an element relative to the
            end of the array.</p>]]></param></params><examples><example name="Basic Usage"><![CDATA[import System;

[ 1, 2, 3, 4, 5 ].slice(0, 2); // [ 1, 2 ]]]></example><example name="Extract the first to penultimate element"><![CDATA[import System;

[ 1, 2, 3, 4, 5 ].slice(0, -1); // [ 1, 2, 3, 4 ]]]></example></examples><see></see></overload></method>